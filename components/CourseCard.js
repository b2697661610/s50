// [S50 ACTIVITY]
import { Button, Row, Col, Card } from 'react-bootstrap';
// [S50 ACTIVITY END]


// [S50 ACTIVITY]
export default function CourseCard() {
return (
    <Row className="mt-3 mb-3">
            <Col xs={12}>
                <Card className="cardHighlight p-0">
                    <Card.Body>
                        <Card.Title>
                            <h4>Sample Course</h4>
                        </Card.Title>
                        <Card.Text>
                          <p>Description:</p>
                          <p>This is a sample course offering</p>
                          <p>Price:</p>
                          <p>PHP 40,000</p>
                        </Card.Text>
                        <Button variant="primary">Enroll</Button>
                    </Card.Body>
                </Card>
            </Col>
    </Row>        
	)
}
// [S50 ACTIVITY END]
